﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DI.Application.Implementation;
using DI.Application.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace DI.Presentation.Controllers
{
    public class OperationController : Controller
    {
        private readonly OperationService _operationService;
        private readonly IOperationTransient _transientOperation;
        private readonly IOperationScoped _scopedOperation;
        private readonly IOperationSingleton _singletonOperation;
        private readonly IOperationSingletonInstance _singletonInstanceOperation;

        public OperationController(OperationService operationService, IOperationTransient transientOperation, IOperationScoped scopedOperation, IOperationSingleton singletonOperation, IOperationSingletonInstance singletonInstanceOperation)
        {
            _operationService = operationService;
            _transientOperation = transientOperation;
            _scopedOperation = scopedOperation;
            _singletonOperation = singletonOperation;
            _singletonInstanceOperation = singletonInstanceOperation;
        }

        public IActionResult Index()
        {
            // ViewBag contains controller-requested services
            ViewBag.Transient = _transientOperation.OperationId;
            ViewBag.Scoped = _scopedOperation.OperationId;
            ViewBag.Singleton = _singletonOperation.OperationId;
            ViewBag.SingletonInstance = _singletonInstanceOperation.OperationId;

            // Operation service has its own requested services
            ViewBag.ServiceTransient = _operationService.TransientOperation.OperationId;
            ViewBag.ServiceScoped = _operationService.ScopedOperation.OperationId;
            ViewBag.ServiceSingleton = _operationService.SingletonOperation.OperationId;
            ViewBag.ServiceSingletonInstance = _operationService.SingletonInstanceOperation.OperationId;
            return View();
        }
    }
}
