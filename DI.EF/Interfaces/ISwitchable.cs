﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EG.Data.Interfaces
{
    public interface ISwitchable
    {
        bool Status { set; get; }
    }
}
